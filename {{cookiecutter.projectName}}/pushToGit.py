import subprocess

gitGroupName = input("Enter GitLab username: ")
gitRepoName = input("Enter GitLab repository name: ")

initGit = subprocess.run(["git", "init"])
addToGit = subprocess.run(["git", "add", "."])
commitToGit = subprocess.run(["git", "commit", "-m", "Initial commit"])
pushToGit = subprocess.run(["git", "push", "--set-upstream", "https://gitlab.com/{}/{}.git".format(gitGroupName, gitRepoName), "master"])

print("Process ended with code: %d" % pushToGit.returncode)