# PythonTemplate


## Getting started

To start your project install cookiecutter library in your venv by executing the following command:

pip install cookiecutter

## Set template

To install template execute it by using PythonTemplate repository HTTPS:

cookiecutter https://gitlab.com/advancedpython1/PythonTemplate.git

## Submit template to GitLab under your group by simply running pushToGit.py

python pushToGit.py
